<h1>Hi, I'm Rooney! <img width="35rem" src="https://raw.githubusercontent.com/TheDudeThatCode/TheDudeThatCode/master/Assets/Hi.gif"></h1>

[![LinkedIn][linkedin-shield]][linkedin-url]
[![GitHub][github-shield]][github-url]


[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=plastic&logo=linkedin&colorB=informational
[linkedin-url]: https://www.linkedin.com/in/rooney-h-osman/
[github-shield]: https://img.shields.io/badge/-github?label=GitHub&style=plastic&logo=github&colorB=lightgrey
[github-url]: https://github.com/os-rooney
